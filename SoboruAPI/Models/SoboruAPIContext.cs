﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SoboruAPI.Models
{
    public class SoboruAPIContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public SoboruAPIContext() : base("name=SoboruAPIContext")
        {
        }

        public System.Data.Entity.DbSet<Soboru.Models.Ingrediente> Ingredientes { get; set; }

        public System.Data.Entity.DbSet<Soboru.Models.Utensilio> Utensilios { get; set; }

        public System.Data.Entity.DbSet<Soboru.Models.Medida> Medidas { get; set; }

        public System.Data.Entity.DbSet<Soboru.Models.Categoria> Categorias { get; set; }

        public System.Data.Entity.DbSet<Soboru.Models.Usuario> Usuarios { get; set; }

        public System.Data.Entity.DbSet<Soboru.Models.Role> Roles { get; set; }
        
        public System.Data.Entity.DbSet<Soboru.Models.Comentario> Comentarios { get; set; }

        public System.Data.Entity.DbSet<Soboru.Models.Receita> Receitas { get; set; }

        public System.Data.Entity.DbSet<Soboru.Models.Pontuacao> Pontuacaos { get; set; }

        public System.Data.Entity.DbSet<Soboru.Models.ReceitaIngrediente> ReceitaIngredientes { get; set; }
    }
}
