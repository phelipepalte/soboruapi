﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Soboru.Models
{
    public class Comentario
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public String BodyComentario { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        //Foreign Key Many-to-One
        [Required]
        public int ReceitaId { get; set; }
        public Receita Receita { get; set; }
        [Required]
        public Usuario Usuario { get; set; }
    }
}