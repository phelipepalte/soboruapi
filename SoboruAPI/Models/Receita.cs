﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Soboru.Models
{
    public class Receita
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public int Porcao { get; set; }
        [Required]
        public float TempoPreparo { get; set; }
        [Required]
        public string ModoPreparo { get; set; }
        public string ImgPath { get; set; } = "";
        public float PontuacaoMedia { get; set; } = 0;
        public int Views { get; set; } = 0;
        [Required]
        public string Slug { get; set; }
        [Required]
        public bool Aprovado { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        //Foreign key Many-to-One
        [Required]
        public Usuario Usuario { get; set; }
        [Required]        
        public Categoria Categoria { get; set; }

        //Listas One-to-Many
        public virtual ICollection<ReceitaIngrediente> ReceitaIngredientes { get; set; }
        public virtual ICollection<Pontuacao> Pontuacoes { get; set; }
        public virtual ICollection<Utensilio> Utensilios { get; set; }
        public virtual ICollection<Comentario> Comentarios { get; set; }

    }
}