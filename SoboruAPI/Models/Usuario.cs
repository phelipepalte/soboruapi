﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Soboru.Models
{
    public class Usuario
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Senha { get; set; }
        [DataType(DataType.Date)]
        public DateTime Nasc { get; set; }
        [Required]
        public int Sexo { get; set; }
        [Required]
        public bool NotificacaoEmail { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        //Foreign Key
        [Required]
        public virtual Role Role { get; set; }

        //Lista One-to-Many
        //public virtual ICollection<Receita> Receitas { get; set; }


    }
}