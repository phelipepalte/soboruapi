﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Soboru.Models
{
    public class Utensilio
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public virtual ICollection<Receita> Receitas { get; set; }
    }
}