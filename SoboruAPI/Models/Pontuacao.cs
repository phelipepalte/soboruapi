﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soboru.Models
{
    [Table("Pontuacoes")]
    public class Pontuacao
    {
        [Key]
        public int Id { get; set; }
        public int QtyPontuacaoReceita { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        //Foreign Key
        [Required]
        public int ReceitaId { get; set; }
        public virtual Receita Receita { get; set; }
        [Required]
        public virtual Usuario Usuario { get; set; }
    }
}