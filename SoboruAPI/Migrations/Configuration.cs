namespace SoboruAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Soboru.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<SoboruAPI.Models.SoboruAPIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SoboruAPI.Models.SoboruAPIContext context)
        {
            context.Categorias.AddOrUpdate(x => x.Id,
                new Categoria() { Id = 1, Nome = "Salgados",     Selecionavel = false,  SuperCategoriaId = null, Slug = "Salgados", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },                
                new Categoria() { Id = 2, Nome = "Tortas",       Selecionavel = true,   SuperCategoriaId = 1,    Slug = "Tortas", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Categoria() { Id = 3, Nome = "Petiscos",     Selecionavel = true,   SuperCategoriaId = 1,    Slug = "Petiscos", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Categoria() { Id = 4, Nome = "Cotidiano",    Selecionavel = true,   SuperCategoriaId = 1,    Slug = "Cotidiano", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Categoria() { Id = 5, Nome = "Gourmet",      Selecionavel = false,  SuperCategoriaId = null, Slug = "Gourmet", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Categoria() { Id = 6, Nome = "Petit gateau", Selecionavel = true,   SuperCategoriaId = 5,    Slug = "Petit gateau", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null }
                );

            context.Ingredientes.AddOrUpdate(x => x.Id,
                new Ingrediente() { Id = 1, Nome = "Feij�o", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null},
                new Ingrediente() { Id = 2, Nome = "Arroz", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Ingrediente() { Id = 3, Nome = "Ovo", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Ingrediente() { Id = 4, Nome = "Farinha", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null }
                );

            context.Medidas.AddOrUpdate(x => x.Id,                
                new Medida() { Id = 1,  Nome = "litro",                 Abreviacao = "L", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null},
                new Medida() { Id = 2,  Nome = "gramas",                Abreviacao = "g", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 3,  Nome = "dentes",                Abreviacao = "dentes", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 4,  Nome = "fio",                   Abreviacao = "fio", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 5,  Nome = "a gosto",               Abreviacao = "a gosto", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 6,  Nome = "unidade",               Abreviacao = "uni", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 7,  Nome = "xicara (cha)",          Abreviacao = "xic", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 8,  Nome = "colher (sopa)",         Abreviacao = "csp", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 9,  Nome = "copo",                  Abreviacao = "cp", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 10, Nome = "fatias",                Abreviacao = "fatias", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 11, Nome = "colheres (sobremesa)",  Abreviacao = "csb", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Medida() { Id = 12, Nome = "mililitro",             Abreviacao = "ml", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null }
                );


            context.Utensilios.AddOrUpdate(x => x.Id,
                new Utensilio() { Id = 1, Nome = "Esp�tula", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Utensilio() { Id = 2, Nome = "Faca", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Utensilio() { Id = 3, Nome = "Batedeira", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Utensilio() { Id = 4, Nome = "Concha", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null }
                );

            context.Roles.AddOrUpdate(x => x.Id,
                new Role() { Id = 1, Nome = "Administrador", isAdmin = true, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Role() { Id = 2, Nome = "Usuario", isAdmin = false,CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null }
                );
            /*
            context.Usuarios.AddOrUpdate(x => x.Id,
                new Usuario() { Id = 1, Nome = "Admin", Email = "admin@admin.com", Senha = "teste", Nasc = DateTime.Now, NotificacaoEmail = true, Role = RoleId = 1, Sexo = 1, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null },
                new Usuario() { Id = 2, Nome = "Usuario", Email = "admin@admin.com", Senha = "teste", Nasc = DateTime.Now, NotificacaoEmail = true, RoleId = 2, Sexo = 1, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, DeletedAt = null }
                );
                */
        }
    }
}
