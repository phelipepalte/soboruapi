namespace SoboruAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Utensilios", "Receita_Id", "dbo.Receitas");
            DropIndex("dbo.Utensilios", new[] { "Receita_Id" });
            RenameColumn(table: "dbo.Comentarios", name: "UsuarioId", newName: "Usuario_Id");
            RenameColumn(table: "dbo.Receitas", name: "UsuarioId", newName: "Usuario_Id");
            RenameColumn(table: "dbo.Pontuacoes", name: "UsuarioId", newName: "Usuario_Id");
            RenameColumn(table: "dbo.Usuarios", name: "RoleId", newName: "Role_Id");
            RenameColumn(table: "dbo.ReceitaIngredientes", name: "IngredienteId", newName: "Ingrediente_Id");
            RenameColumn(table: "dbo.ReceitaIngredientes", name: "MedidaId", newName: "Medida_Id");
            RenameColumn(table: "dbo.Receitas", name: "CategoriaId", newName: "Categoria_Id");
            RenameIndex(table: "dbo.Receitas", name: "IX_CategoriaId", newName: "IX_Categoria_Id");
            RenameIndex(table: "dbo.Receitas", name: "IX_UsuarioId", newName: "IX_Usuario_Id");
            RenameIndex(table: "dbo.Comentarios", name: "IX_UsuarioId", newName: "IX_Usuario_Id");
            RenameIndex(table: "dbo.Usuarios", name: "IX_RoleId", newName: "IX_Role_Id");
            RenameIndex(table: "dbo.Pontuacoes", name: "IX_UsuarioId", newName: "IX_Usuario_Id");
            RenameIndex(table: "dbo.ReceitaIngredientes", name: "IX_IngredienteId", newName: "IX_Ingrediente_Id");
            RenameIndex(table: "dbo.ReceitaIngredientes", name: "IX_MedidaId", newName: "IX_Medida_Id");
            CreateTable(
                "dbo.UtensilioReceitas",
                c => new
                    {
                        Utensilio_Id = c.Int(nullable: false),
                        Receita_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Utensilio_Id, t.Receita_Id })
                .ForeignKey("dbo.Utensilios", t => t.Utensilio_Id, cascadeDelete: true)
                .ForeignKey("dbo.Receitas", t => t.Receita_Id, cascadeDelete: true)
                .Index(t => t.Utensilio_Id)
                .Index(t => t.Receita_Id);
            
            AlterColumn("dbo.Comentarios", "BodyComentario", c => c.String(nullable: false));
            DropColumn("dbo.Utensilios", "Receita_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Utensilios", "Receita_Id", c => c.Int());
            DropForeignKey("dbo.UtensilioReceitas", "Receita_Id", "dbo.Receitas");
            DropForeignKey("dbo.UtensilioReceitas", "Utensilio_Id", "dbo.Utensilios");
            DropIndex("dbo.UtensilioReceitas", new[] { "Receita_Id" });
            DropIndex("dbo.UtensilioReceitas", new[] { "Utensilio_Id" });
            AlterColumn("dbo.Comentarios", "BodyComentario", c => c.String());
            DropTable("dbo.UtensilioReceitas");
            RenameIndex(table: "dbo.ReceitaIngredientes", name: "IX_Medida_Id", newName: "IX_MedidaId");
            RenameIndex(table: "dbo.ReceitaIngredientes", name: "IX_Ingrediente_Id", newName: "IX_IngredienteId");
            RenameIndex(table: "dbo.Pontuacoes", name: "IX_Usuario_Id", newName: "IX_UsuarioId");
            RenameIndex(table: "dbo.Usuarios", name: "IX_Role_Id", newName: "IX_RoleId");
            RenameIndex(table: "dbo.Comentarios", name: "IX_Usuario_Id", newName: "IX_UsuarioId");
            RenameIndex(table: "dbo.Receitas", name: "IX_Usuario_Id", newName: "IX_UsuarioId");
            RenameIndex(table: "dbo.Receitas", name: "IX_Categoria_Id", newName: "IX_CategoriaId");
            RenameColumn(table: "dbo.Receitas", name: "Categoria_Id", newName: "CategoriaId");
            RenameColumn(table: "dbo.ReceitaIngredientes", name: "Medida_Id", newName: "MedidaId");
            RenameColumn(table: "dbo.ReceitaIngredientes", name: "Ingrediente_Id", newName: "IngredienteId");
            RenameColumn(table: "dbo.Usuarios", name: "Role_Id", newName: "RoleId");
            RenameColumn(table: "dbo.Pontuacoes", name: "Usuario_Id", newName: "UsuarioId");
            RenameColumn(table: "dbo.Receitas", name: "Usuario_Id", newName: "UsuarioId");
            RenameColumn(table: "dbo.Comentarios", name: "Usuario_Id", newName: "UsuarioId");
            CreateIndex("dbo.Utensilios", "Receita_Id");
            AddForeignKey("dbo.Utensilios", "Receita_Id", "dbo.Receitas", "Id");
        }
    }
}
