﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Soboru.Models;
using SoboruAPI.Models;

namespace SoboruAPI.Controllers
{
    public class ReceitaIngredientesController : ApiController
    {
        private SoboruAPIContext db = new SoboruAPIContext();

        // GET: api/ReceitaIngredientes
        public IQueryable<ReceitaIngrediente> GetReceitaIngredientes()
        {
            return db.ReceitaIngredientes;
        }

        // GET: api/ReceitaIngredientes/5
        [ResponseType(typeof(ReceitaIngrediente))]
        public async Task<IHttpActionResult> GetReceitaIngrediente(int id)
        {
            ReceitaIngrediente receitaIngrediente = await db.ReceitaIngredientes.FindAsync(id);
            if (receitaIngrediente == null)
            {
                return NotFound();
            }

            return Ok(receitaIngrediente);
        }

        // PUT: api/ReceitaIngredientes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutReceitaIngrediente(int id, ReceitaIngrediente receitaIngrediente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != receitaIngrediente.Id)
            {
                return BadRequest();
            }

            db.Entry(receitaIngrediente).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReceitaIngredienteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ReceitaIngredientes
        [ResponseType(typeof(ReceitaIngrediente))]
        public async Task<IHttpActionResult> PostReceitaIngrediente(ReceitaIngrediente receitaIngrediente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ReceitaIngredientes.Add(receitaIngrediente);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = receitaIngrediente.Id }, receitaIngrediente);
        }

        // DELETE: api/ReceitaIngredientes/5
        [ResponseType(typeof(ReceitaIngrediente))]
        public async Task<IHttpActionResult> DeleteReceitaIngrediente(int id)
        {
            ReceitaIngrediente receitaIngrediente = await db.ReceitaIngredientes.FindAsync(id);
            if (receitaIngrediente == null)
            {
                return NotFound();
            }

            db.ReceitaIngredientes.Remove(receitaIngrediente);
            await db.SaveChangesAsync();

            return Ok(receitaIngrediente);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReceitaIngredienteExists(int id)
        {
            return db.ReceitaIngredientes.Count(e => e.Id == id) > 0;
        }
    }
}