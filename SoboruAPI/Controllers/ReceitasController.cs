﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Soboru.Models;
using SoboruAPI.Models;

namespace SoboruAPI.Controllers
{
    public class ReceitasController : ApiController
    {
        private SoboruAPIContext db = new SoboruAPIContext();

        // GET: api/Receitas
        public IQueryable<Receita> GetReceitas()
        {
            return db.Receitas;
        }

        // GET: api/Receitas/5
        [ResponseType(typeof(Receita))]
        public async Task<IHttpActionResult> GetReceita(int id)
        {
            Receita receita = await db.Receitas.FindAsync(id);
            if (receita == null)
            {
                return NotFound();
            }

            return Ok(receita);
        }

        // PUT: api/Receitas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutReceita(int id, Receita receita)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != receita.Id)
            {
                return BadRequest();
            }

            db.Entry(receita).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReceitaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Receitas
        [ResponseType(typeof(Receita))]
        public async Task<IHttpActionResult> PostReceita(Receita receita)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Receitas.Add(receita);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = receita.Id }, receita);
        }

        // DELETE: api/Receitas/5
        [ResponseType(typeof(Receita))]
        public async Task<IHttpActionResult> DeleteReceita(int id)
        {
            Receita receita = await db.Receitas.FindAsync(id);
            if (receita == null)
            {
                return NotFound();
            }

            db.Receitas.Remove(receita);
            await db.SaveChangesAsync();

            return Ok(receita);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReceitaExists(int id)
        {
            return db.Receitas.Count(e => e.Id == id) > 0;
        }
    }
}