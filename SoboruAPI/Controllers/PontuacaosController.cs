﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Soboru.Models;
using SoboruAPI.Models;

namespace SoboruAPI.Controllers
{
    public class PontuacaosController : ApiController
    {
        private SoboruAPIContext db = new SoboruAPIContext();

        // GET: api/Pontuacaos
        public IQueryable<Pontuacao> GetPontuacaos()
        {
            return db.Pontuacaos;
        }

        // GET: api/Pontuacaos/5
        [ResponseType(typeof(Pontuacao))]
        public async Task<IHttpActionResult> GetPontuacao(int id)
        {
            Pontuacao pontuacao = await db.Pontuacaos.FindAsync(id);
            if (pontuacao == null)
            {
                return NotFound();
            }

            return Ok(pontuacao);
        }

        // PUT: api/Pontuacaos/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPontuacao(int id, Pontuacao pontuacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pontuacao.Id)
            {
                return BadRequest();
            }

            db.Entry(pontuacao).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PontuacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pontuacaos
        [ResponseType(typeof(Pontuacao))]
        public async Task<IHttpActionResult> PostPontuacao(Pontuacao pontuacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Pontuacaos.Add(pontuacao);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = pontuacao.Id }, pontuacao);
        }

        // DELETE: api/Pontuacaos/5
        [ResponseType(typeof(Pontuacao))]
        public async Task<IHttpActionResult> DeletePontuacao(int id)
        {
            Pontuacao pontuacao = await db.Pontuacaos.FindAsync(id);
            if (pontuacao == null)
            {
                return NotFound();
            }

            db.Pontuacaos.Remove(pontuacao);
            await db.SaveChangesAsync();

            return Ok(pontuacao);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PontuacaoExists(int id)
        {
            return db.Pontuacaos.Count(e => e.Id == id) > 0;
        }
    }
}