﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Soboru.Models;
using SoboruAPI.Models;

namespace SoboruAPI.Controllers
{
    public class IngredientesController : ApiController
    {
        private SoboruAPIContext db = new SoboruAPIContext();

        // GET: api/Ingredientes
        public IQueryable<Ingrediente> GetIngredientes()
        {
            return db.Ingredientes;
        }

        // GET: api/Ingredientes/5
        [ResponseType(typeof(Ingrediente))]
        public async Task<IHttpActionResult> GetIngrediente(int id)
        {
            Ingrediente ingrediente = await db.Ingredientes.FindAsync(id);
            if (ingrediente == null)
            {
                return NotFound();
            }

            return Ok(ingrediente);
        }

        // PUT: api/Ingredientes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutIngrediente(int id, Ingrediente ingrediente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ingrediente.Id)
            {
                return BadRequest();
            }

            db.Entry(ingrediente).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IngredienteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Ingredientes
        [ResponseType(typeof(Ingrediente))]
        public async Task<IHttpActionResult> PostIngrediente(Ingrediente ingrediente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Ingredientes.Add(ingrediente);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = ingrediente.Id }, ingrediente);
        }

        // DELETE: api/Ingredientes/5
        [ResponseType(typeof(Ingrediente))]
        public async Task<IHttpActionResult> DeleteIngrediente(int id)
        {
            Ingrediente ingrediente = await db.Ingredientes.FindAsync(id);
            if (ingrediente == null)
            {
                return NotFound();
            }

            db.Ingredientes.Remove(ingrediente);
            await db.SaveChangesAsync();

            return Ok(ingrediente);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IngredienteExists(int id)
        {
            return db.Ingredientes.Count(e => e.Id == id) > 0;
        }
    }
}