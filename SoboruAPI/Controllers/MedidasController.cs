﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Soboru.Models;
using SoboruAPI.Models;

namespace SoboruAPI.Controllers
{
    public class MedidasController : ApiController
    {
        private SoboruAPIContext db = new SoboruAPIContext();

        // GET: api/Medidas
        public IQueryable<Medida> GetMedidas()
        {
            return db.Medidas;
        }

        // GET: api/Medidas/5
        [ResponseType(typeof(Medida))]
        public async Task<IHttpActionResult> GetMedida(int id)
        {
            Medida medida = await db.Medidas.FindAsync(id);
            if (medida == null)
            {
                return NotFound();
            }

            return Ok(medida);
        }

        // PUT: api/Medidas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMedida(int id, Medida medida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medida.Id)
            {
                return BadRequest();
            }

            db.Entry(medida).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedidaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Medidas
        [ResponseType(typeof(Medida))]
        public async Task<IHttpActionResult> PostMedida(Medida medida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Medidas.Add(medida);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = medida.Id }, medida);
        }

        // DELETE: api/Medidas/5
        [ResponseType(typeof(Medida))]
        public async Task<IHttpActionResult> DeleteMedida(int id)
        {
            Medida medida = await db.Medidas.FindAsync(id);
            if (medida == null)
            {
                return NotFound();
            }

            db.Medidas.Remove(medida);
            await db.SaveChangesAsync();

            return Ok(medida);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MedidaExists(int id)
        {
            return db.Medidas.Count(e => e.Id == id) > 0;
        }
    }
}