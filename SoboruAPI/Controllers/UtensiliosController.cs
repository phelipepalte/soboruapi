﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Soboru.Models;
using SoboruAPI.Models;

namespace SoboruAPI.Controllers
{
    public class UtensiliosController : ApiController
    {
        private SoboruAPIContext db = new SoboruAPIContext();

        // GET: api/Utensilios
        public IQueryable<Utensilio> GetUtensilios()
        {
            return db.Utensilios;
        }

        // GET: api/Utensilios/5
        [ResponseType(typeof(Utensilio))]
        public async Task<IHttpActionResult> GetUtensilio(int id)
        {
            Utensilio utensilio = await db.Utensilios.FindAsync(id);
            if (utensilio == null)
            {
                return NotFound();
            }

            return Ok(utensilio);
        }

        // PUT: api/Utensilios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUtensilio(int id, Utensilio utensilio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != utensilio.Id)
            {
                return BadRequest();
            }

            db.Entry(utensilio).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UtensilioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Utensilios
        [ResponseType(typeof(Utensilio))]
        public async Task<IHttpActionResult> PostUtensilio(Utensilio utensilio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Utensilios.Add(utensilio);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = utensilio.Id }, utensilio);
        }

        // DELETE: api/Utensilios/5
        [ResponseType(typeof(Utensilio))]
        public async Task<IHttpActionResult> DeleteUtensilio(int id)
        {
            Utensilio utensilio = await db.Utensilios.FindAsync(id);
            if (utensilio == null)
            {
                return NotFound();
            }

            db.Utensilios.Remove(utensilio);
            await db.SaveChangesAsync();

            return Ok(utensilio);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UtensilioExists(int id)
        {
            return db.Utensilios.Count(e => e.Id == id) > 0;
        }
    }
}